interface PropsButton {
    title:string, 
    secundary?:boolean,
    primary?:boolean, 
    border?:boolean
}
export default function Button(props:PropsButton){
    let classDefault:any ="btn"
    const designButton:any ={
        secundary:"btn-nav",
        primary:"btn-primary",
        border:"btn-border"
    }
    
    const setProps:any = JSON.parse(JSON.stringify(props))
    for (const key in setProps) {
        if (props.hasOwnProperty(key) && designButton[key]) {
            classDefault += ` ${designButton[key]}`
        }
    }

    return(
        props.primary?
        <button className={classDefault}>
            <span>{props.title}</span>
        </button>
        :
        <button className={classDefault}>
            <span>{props.title}</span>
        </button>
    )
}   

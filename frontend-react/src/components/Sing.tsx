type SingProps ={
    text:string,
    icon:string,
}

const Sing = (props:SingProps) =>{
    const {text, icon} = props;
    return(
        <div className="sing">
            <i>{icon}</i>
            <span>{text}</span>
        </div>
    )
}

export default Sing;

**Los componentes son los siguientes:**

**Header.tsx**

    -Logo
    -Sing
    -Description
    -Button

**Button.tsx**

![componentes](/img-comp/button.png)

---

**Logo.tsx**

![componentes](/img-comp/logo.png)

---

**Sing.tsx**

![componentes](/img-comp/sing.png)

---

**Images.tsx**

![componentes](/img-comp/image.png)

---

**Icon.tsx**

![componentes](/img-comp/icon.png)

---

**Description.tsx**

![componentes](/img-comp/description.png)

---

**Footer.tsx**

    -Logo
    -Icon



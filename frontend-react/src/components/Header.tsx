import Button from './Button'
import Logo from './Logo'
import Link from 'next/link'

export function Header(){
    return (
        <div className="container nav-header">
            <Logo></Logo>
            <ul>
                <li>
                    <Link href="/about">About</Link>
                </li>
                <li>
                    <Link href="/pricing">Pricing</Link>
                </li>
                <li>
                    <Link href="/contact-us">Contact Us</Link>
                </li>
                <li>
                    <Link href="login">Login</Link>
                </li>
            </ul>
            <div>
                <Button title="Start free trial" border secundary></Button>
            </div>
        </div>
    )
}
import Image from "next/image";
import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Sing from "@/components/Sing";

export default function Home() {
  return (
    <main>
      <div className="bg-header"></div>
      <section className="container header">
        <div>
          <Sing text="v3.1 released. Learn more" icon={""}></Sing>
          <div className="header-title">
            <h1>Your data with real-time analytics</h1>
            <p>Harness the potential of Big Data Analytics & Cloud Services and become a data-driven organization with Needle tail</p>
            <div>
              <Button title="Start free trial" primary></Button>
              <Button title="Learn more" secundary></Button>
            </div>
          </div>
        </div>
        <div>
          <img src="/images/header.svg" alt=""></img>
        </div>
      </section>
    </main>
  );
}

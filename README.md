# FREE LANDING PAGE
**Created by:** Univ. Laura Gisel Quispe Olmedo

## LINK DEL TEMPLATE EN FIGMA
https://www.figma.com/design/9FDhkc42INbrU8RiAP3Irs/Option-1?node-id=0-1&t=0UPI9CI7nnpJjK9y-0

## LINK DEL REPOSITORIO PRINCIPAL
https://gitlab.com/quispeogisell/aux-sis313g1-i24


## AVANCES DEL TEMPLATE
- [X] Header
- [ ] Seccion 1
- [ ] Seccion 2
- [ ] Seccion 3
- [ ] Seccion 4
- [ ] Seccion 5
- [ ] Footer
